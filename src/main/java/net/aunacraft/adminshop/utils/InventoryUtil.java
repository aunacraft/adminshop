package net.aunacraft.adminshop.utils;

import net.aunacraft.api.player.AunaPlayer;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class InventoryUtil {

    public static boolean hasItemInInventoryWithMinAmount(AunaPlayer player, ItemStack stack, int minAmount){
        int amount = 0;
        for (ItemStack itemStack : player.toBukkitPlayer().getInventory().getContents()) if(itemStack.isSimilar(stack)) amount += itemStack.getAmount();
        return (amount >= minAmount);
    }

    public static int getAmountFromItemInInventory(AunaPlayer player, ItemStack stack){
        int amount = 0;
        for (ItemStack itemStack : player.toBukkitPlayer().getInventory().getContents()) if(itemStack.isSimilar(stack)) amount += itemStack.getAmount();
        return amount;
    }

    public static boolean removeFromType(AunaPlayer player, int amountToRemove, ItemStack type){
        if(hasItemInInventoryWithMinAmount(player, type, amountToRemove)){
            AtomicInteger removed = new AtomicInteger(0);
            Arrays.asList(player.toBukkitPlayer().getInventory().getContents()).forEach(stack -> {
                if(removed.get() < amountToRemove){
                    if(stack.isSimilar(stack)){
                        if(stack.getAmount() > amountToRemove){
                            stack.setAmount(stack.getAmount()-amountToRemove);
                            removed.set(amountToRemove);
                        }else{
                            player.toBukkitPlayer().getInventory().remove(stack);
                            removed.addAndGet(stack.getAmount());
                        }
                    }
                }
            });
            return true;
        }
        return false;
    }

}
