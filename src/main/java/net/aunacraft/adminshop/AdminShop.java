package net.aunacraft.adminshop;

import lombok.Getter;
import net.aunacraft.adminshop.commands.AdminShopCommand;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.api.database.config.impl.SpigotDatabaseConfig;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class AdminShop extends JavaPlugin {
    @Getter
    private static AdminShop instance;

    private DatabaseHandler databaseHandler;
    private final String MYSQL_DATABASE_NAME = "admin_shops";

    @Override
    public void onEnable() {
        instance = this;
        this.databaseHandler = new DatabaseHandler(new SpigotDatabaseConfig(this));
        AunaAPI.getApi().registerCommand(new AdminShopCommand());
        this.databaseHandler.createBuilder("CREATE TABLE IF NOT EXISTS " + MYSQL_DATABASE_NAME + "(id TEXT, categoryNameKey TEXT, npcLocation TEXT, uuid TEXT, shopItemList TEXT);").updateSync();
        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
