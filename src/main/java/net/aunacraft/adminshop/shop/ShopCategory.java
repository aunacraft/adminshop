package net.aunacraft.adminshop.shop;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;
import net.aunacraft.adminshop.AdminShop;
import net.aunacraft.adminshop.shop.gui.ShopSiteUI;
import net.aunacraft.adminshop.shop.item.ShopItem;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.bukkit.npc.base.NPCBase;
import net.aunacraft.api.bukkit.npc.builder.NPCBuilder;
import net.aunacraft.api.bukkit.util.LocationUtil;
import net.aunacraft.api.util.SkinFetcher;
import org.bukkit.Location;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

@Getter
public class ShopCategory {

    @Setter
    private String categoryNameKey;
    private NPCBase npc;
    private String ID;
    private UUID skinUUID;
    @Setter
    private CopyOnWriteArrayList<ShopItem> shopItemList;

    public ShopCategory(String ID, String categoryNameKey, Location npcLocation, UUID uuid, CopyOnWriteArrayList<ShopItem> shopItemList) {
        this.shopItemList = shopItemList;
        this.categoryNameKey = categoryNameKey;
        this.ID = ID;
        this.skinUUID = uuid;
        this.npc = new NPCBuilder(AdminShop.getPlugin(AdminShop.class))
                .setInteractListener(npcInteractEvent -> new ShopSiteUI(categoryNameKey, AunaAPI.getApi().getPlayer(npcInteractEvent.getPlayer().getName()), this))
                .setNameKey(categoryNameKey)
                .setSkin(SkinFetcher.getSkinFromUUID(uuid))
                .setLookAtPlayer(true)
                .setLocation(npcLocation)
                .setVisibleType(NPCBase.VisibleType.ALL)
                .build();
    }

    public void update_safe(Runnable callback){
        AdminShop.getInstance().getDatabaseHandler().createBuilder(
                "UPDATE " + AdminShop.getInstance().getMYSQL_DATABASE_NAME() + " SET categoryNameKey = ?, npcLocation = ?, uuid = ?, shopItemList = ? WHERE id = ?")
                .addObjects(categoryNameKey, LocationUtil.locationToString(npc.getLocation()), skinUUID.toString(), new Gson().toJson(shopItemList), ID).updateAsync(callback);
    }

    public void insert_safe(Runnable callback){
        AdminShop.getInstance().getDatabaseHandler().createBuilder(
                "INSERT INTO " + AdminShop.getInstance().getMYSQL_DATABASE_NAME() + "(id, categoryNameKey, npcLocation, uuid, shopItemList) VALUES (?, ?, ?, ?, ?)")
                .addObjects(ID, categoryNameKey, LocationUtil.locationToString(npc.getLocation()), skinUUID.toString(), new Gson().toJson(shopItemList)).updateAsync(callback);
    }

    public static void loadFromMySQL(String ID, Consumer<ShopCategory> consumer){
        AdminShop.getInstance().getDatabaseHandler().createBuilder("SELECT * FROM " + AdminShop.getInstance().getMYSQL_DATABASE_NAME() + " WHERE id = ?").addObjects(ID).queryAsync(cachedRowSet -> {
            try {
                if(cachedRowSet.next()){
                    consumer.accept(
                            new ShopCategory(
                                    ID,
                                    cachedRowSet.getString("categoryNameKey"),
                                    LocationUtil.locationFromString(cachedRowSet.getString("npcLocation")),
                                    UUID.fromString(cachedRowSet.getString("uuid")),
                                    new Gson().fromJson(cachedRowSet.getString("shopItemList"), CopyOnWriteArrayList.class)
                            )
                    );
                }else consumer.accept(null); // Not printing exception cause null can be used in If statements for user oriented error description
            }catch (Exception exception){
                exception.printStackTrace();
            }
        });
    }

    public static void getAllShopCategories(Consumer<List<ShopCategory>> listConsumer){
        AdminShop.getInstance().getDatabaseHandler().createBuilder("SELECT * FROM " + AdminShop.getInstance().getMYSQL_DATABASE_NAME() + "").queryAsync(cachedRowSet -> {
            try {
                List<ShopCategory> list = new CopyOnWriteArrayList<>();
                while (cachedRowSet.next())
                    list.add(
                            new ShopCategory(
                                    cachedRowSet.getString("id"),
                                    cachedRowSet.getString("categoryNameKey"),
                                    LocationUtil.locationFromString(cachedRowSet.getString("npcLocation")),
                                    UUID.fromString(cachedRowSet.getString("uuid")),
                                    new Gson().fromJson(cachedRowSet.getString("shopItemList"), CopyOnWriteArrayList.class)
                            )
                    );
                listConsumer.accept(list);
            }catch (Exception exception){
                exception.printStackTrace();
            }
        });
    }

}
