package net.aunacraft.adminshop.shop.gui;

import net.aunacraft.adminshop.shop.ShopCategory;
import net.aunacraft.adminshop.shop.item.ShopItem;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class ShopSiteEditUI extends Gui {

    private AunaPlayer player;
    private ShopCategory shopCategory;

    public ShopSiteEditUI(AunaPlayer player, ShopCategory category) {
        super(player.getMessage(category.getCategoryNameKey()), 9*6);
        this.player = player;
        this.shopCategory = category;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName(" ").addItemFlags(ItemFlag.HIDE_ATTRIBUTES).build());
        AtomicInteger slot = new AtomicInteger(0);
        shopCategory.getShopItemList().forEach(shopItem -> {
            guiInventory.setItem(slot.get(), shopItem.toIcon(this.player.getMessageLanguage()));
            slot.getAndIncrement();
        });
    }

    @Override
    public void onClose(Player player, InventoryCloseEvent event) {
        CopyOnWriteArrayList<ShopItem> shopItemList = new CopyOnWriteArrayList<>();
        for(ItemStack stack : event.getInventory().getContents()) shopItemList.add(ShopItem.getItemWithIconItemStack(stack));
        shopCategory.setShopItemList(shopItemList);
        shopCategory.update_safe(() -> this.player.sendMessage("aunashop.editsite.succesfullysaved"));
        super.onClose(player, event);
    }
}
