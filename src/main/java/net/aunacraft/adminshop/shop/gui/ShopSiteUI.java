package net.aunacraft.adminshop.shop.gui;

import net.aunacraft.adminshop.shop.ShopCategory;
import net.aunacraft.adminshop.shop.item.ShopItem;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ShopSiteUI extends Gui {

    private AunaPlayer player;
    private ShopCategory shopCategory;

    public ShopSiteUI(String guiNameKey, AunaPlayer player, ShopCategory category) {
        super(player.getMessage(guiNameKey), 9*6);
        this.player = player;
        this.shopCategory = category;
    }

    @Override
    public void initInventory(Player player, GuiInventory guiInventory) {
        guiInventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName(" ").addItemFlags(ItemFlag.HIDE_ATTRIBUTES).build());
        AtomicInteger slot = new AtomicInteger(0);
        shopCategory.getShopItemList().forEach(shopItem -> {
            guiInventory.setItem(slot.get(), shopItem.toIcon(this.player.getMessageLanguage()), event -> {
                if(event.isLeftClick()){
                    if(event.isShiftClick()) {
                        shopItem.sellAll(this.player, shopCategory);
                    }else shopItem.sell(this.player, shopCategory);
                }else if(event.isRightClick()){
                    shopItem.buy(this.player, shopCategory);
                }
            });
            slot.getAndIncrement();
        });
    }
}
