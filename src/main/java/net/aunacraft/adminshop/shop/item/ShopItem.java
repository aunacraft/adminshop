package net.aunacraft.adminshop.shop.item;

import lombok.Getter;
import lombok.val;
import net.aunacraft.adminshop.shop.ShopCategory;
import net.aunacraft.adminshop.utils.InventoryUtil;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.backend.message.MessageLanguage;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

@Getter
public class ShopItem {

    private double buyPrice;
    private double sellPrice;
    private int amountPerPrice;
    private ItemStack item;

    public ShopItem(double buyPrice, double sellPrice, int amountPerPrice, ItemStack item) {
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
        this.amountPerPrice = amountPerPrice;
        this.item = item;
    }

    public ItemStack toIcon(MessageLanguage language){
        ItemStack itemStack = new ItemBuilder(item.clone())
                .setNBT(compound -> {
                    compound.setDouble("shop_buyPrice", buyPrice);
                    compound.setDouble("shop_sellPrice", sellPrice);
                    compound.setInt("shop_amountPerPrice", amountPerPrice);
                    return compound;
                })
                .setLore(" ",
                        AunaAPI.getApi().getMessageService().getMessage("aunashop.item.buypricelore", language, buyPrice),
                        AunaAPI.getApi().getMessageService().getMessage("aunashop.item.sellpricelore", language, sellPrice),
                        AunaAPI.getApi().getMessageService().getMessage("aunashop.item.descriptionlore", language))
                .addItemFlags(ItemFlag.HIDE_ATTRIBUTES)
                .build();

        return null;
    }

    public boolean buy(AunaPlayer player, ShopCategory shopCategory){
        if(player.getCoins() >= buyPrice){
            if(player.toBukkitPlayer().getInventory().getSize() > player.toBukkitPlayer().getInventory().getContents().length){
                AunaAPI.getApi().setCoins(player.getUuid(), player.getCoins() - buyPrice);
                ItemStack finalStack = item.clone();
                finalStack.setAmount(amountPerPrice);
                player.toBukkitPlayer().getInventory().addItem(finalStack);
                player.sendMessage("aunashop.item.buy.succesfull", amountPerPrice, player.getMessage(shopCategory.getCategoryNameKey()), buyPrice);
                return true;
            }else{
                player.sendMessage("aunashop.item.buy.noplaceininventory");
                return false;
            }
        }else{
            player.sendMessage("aunashop.item.buy.notenoughmoney");
        }
        return false;
    }

    public boolean sell(AunaPlayer player, ShopCategory shopCategory){
        if(InventoryUtil.hasItemInInventoryWithMinAmount(player, item, amountPerPrice)){
            AunaAPI.getApi().setCoins(player.getUuid(), player.getCoins() + sellPrice);
            InventoryUtil.removeFromType(player, amountPerPrice, item);
            player.sendMessage("aunashop.item.sell.succesfull", amountPerPrice, player.getMessage(shopCategory.getCategoryNameKey()), sellPrice);
            return true;
        }else{
            player.sendMessage("aunashop.item.notenoughitems");
        }
        return false;
    }

    public boolean sellAll(AunaPlayer player, ShopCategory shopCategory){
        int amount = InventoryUtil.getAmountFromItemInInventory(player, item);
        if(amount <= 0){
            player.sendMessage("aunashop.item.notenoughitems");
            return false;
        }
        if(InventoryUtil.hasItemInInventoryWithMinAmount(player, item, amount)){
            AunaAPI.getApi().setCoins(player.getUuid(), player.getCoins() + (sellPrice / amountPerPrice)*amount);
            InventoryUtil.removeFromType(player, amount, item);
            player.sendMessage("aunashop.item.sell.succesfull", amount, player.getMessage(shopCategory.getCategoryNameKey()), (sellPrice / amountPerPrice)*amount);
            return true;
        }else{
            player.sendMessage("aunashop.item.notenoughitems");
        }
        return false;
    }

    public static ShopItem getItemWithIconItemStack(ItemStack stack){
        ShopItem item = null;

        final ItemBuilder builder = new ItemBuilder(stack);
        val compound = builder.toNMSStack().getTag();

        if(compound.hasKey("shop_buyPrice")){
            item = new ShopItem(
                    compound.getDouble("shop_buyPrice"),
                    compound.getDouble("shop_sellPrice"),
                    compound.getInt("shop_amountPerPrice"),
                    builder.setNBT(itemCompound -> {
                        itemCompound.remove("shop_buyPrice");
                        itemCompound.remove("shop_sellPrice");
                        itemCompound.remove("shop_amountPerPrice");
                        return itemCompound;
                    }).build()
            );
        }

        return item;
    }

    public static boolean isInstance(ItemStack stack){
        return getItemWithIconItemStack(stack) != null;
    }
}
