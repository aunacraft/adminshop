package net.aunacraft.adminshop.commands;

import net.aunacraft.adminshop.shop.ShopCategory;
import net.aunacraft.adminshop.shop.gui.ShopSiteEditUI;
import net.aunacraft.adminshop.shop.item.ShopItem;
import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.autocompleetion.impl.DoubleCompletionHandler;
import net.aunacraft.api.commands.autocompleetion.impl.IntegerCompletionHandler;
import net.aunacraft.api.commands.autocompleetion.impl.NoCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Material;

import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class AdminShopCommand implements AunaCommand {
    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("adminshop")
                .subCommand(
                        CommandBuilder.beginCommand("edit")
                                .permission("adminshop.edit")
                                .aliases("Allows you to edit a specific Shop")
                                .parameter(ParameterBuilder.beginParameter("shopid").required().autoCompletionHandler(new NoCompletionHandler()).build())
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    ShopCategory.loadFromMySQL(commandContext.getParameterValue("shopid", String.class), category -> {
                                        if(category == null){
                                            aunaPlayer.sendMessage("aunashop.command.edit.notfound");
                                            return;
                                        }
                                        new ShopSiteEditUI(aunaPlayer, category).open(aunaPlayer.toBukkitPlayer());
                                    });
                                }).build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("list")
                                .permission("adminshop.list")
                                .description("Shows all admin shop categories")
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    ShopCategory.getAllShopCategories(shopCategories -> {
                                        if(shopCategories.isEmpty()){
                                            aunaPlayer.sendMessage("aunashop.command.list.empty");
                                            return;
                                        }
                                        aunaPlayer.sendMessage("aunashop.command.list.header");
                                        for (ShopCategory shopCategory : shopCategories){
                                            var component = new TextComponent(aunaPlayer.getMessage("aunashop.command.list.item", shopCategory.getID()));
                                            component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(aunaPlayer.getMessage("aunashop.command.list.item.hover"))));
                                            component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "adminshop edit " + shopCategory.getID()));
                                            aunaPlayer.toBukkitPlayer().spigot().sendMessage(component);
                                        }
                                    });
                                }).build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("createitem")
                                .permission("adminshop.edit")
                                .description("Transfers the item in your hand into a ShopItem")
                                .parameter(ParameterBuilder.beginParameter("amountperprice").autoCompletionHandler(new IntegerCompletionHandler()).required().build())
                                .parameter(ParameterBuilder.beginParameter("buyprice").autoCompletionHandler(new DoubleCompletionHandler()).required().build())
                                .parameter(ParameterBuilder.beginParameter("sellprice").autoCompletionHandler(new DoubleCompletionHandler()).required().build())
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    if(aunaPlayer.toBukkitPlayer().getItemInHand().getType() == Material.AIR){
                                        return;
                                    }
                                    ShopItem shopItem = new ShopItem(commandContext.getParameterValue("buyprice", Double.class),
                                            commandContext.getParameterValue("sellprice", Double.class),
                                            commandContext.getParameterValue("amountperprice", Integer.class), aunaPlayer.toBukkitPlayer().getItemInHand());
                                    aunaPlayer.toBukkitPlayer().getInventory().addItem(shopItem.toIcon(aunaPlayer.getMessageLanguage()));
                                    aunaPlayer.sendMessage("aunashop.command.created");
                                }).build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("createshop")
                                .permission("adminshop.edit")
                                .description("Creates a new Shop Category")
                                .parameter(ParameterBuilder.beginParameter("shopID").autoCompletionHandler(new NoCompletionHandler()).required().build())
                                .parameter(ParameterBuilder.beginParameter("categoryNameKey").autoCompletionHandler(new NoCompletionHandler()).required().build())
                                .parameter(ParameterBuilder.beginParameter("skinUUID").autoCompletionHandler(new NoCompletionHandler()).required().build())
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    aunaPlayer.sendRawMessage("Creating...");
                                    ShopCategory category = new ShopCategory(commandContext.getParameterValue("shopID", String.class),
                                            commandContext.getParameterValue("categoryNameKey", String.class), aunaPlayer.toBukkitPlayer().getLocation(),
                                            UUID.fromString(commandContext.getParameterValue("skinUUID", String.class)), new CopyOnWriteArrayList<>());
                                    category.insert_safe(() -> {
                                        aunaPlayer.sendRawMessage("Created!");
                                    });
                                }).build()
                )
                .handler((aunaPlayer, commandContext, strings) -> {
                    // TODO Do some stuff
                })
                .build();
    }
}
